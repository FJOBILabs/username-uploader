package de.fjobilabs.usernamedatabasetools.reader;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 16.12.2017 - 11:34:17
 */
public class UsernameChunk {
    
    private int id;
    private Collection<String> usernames;
    
    public UsernameChunk(int id, Collection<String> usernames) {
        this.id = id;
        this.usernames = Collections.unmodifiableCollection(usernames);
    }
    
    public int getId() {
        return id;
    }
    
    public int getSize() {
        return usernames.size();
    }
    
    public Collection<String> getUsernames() {
        return usernames;
    }
}
