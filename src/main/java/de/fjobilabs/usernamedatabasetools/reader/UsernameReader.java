package de.fjobilabs.usernamedatabasetools.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Scanner;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 16.12.2017 - 11:22:15
 */
public class UsernameReader {
    
    private File sourceFile;
    private Scanner scanner;
    private int chunkSize;
    private int currentLine;
    private int currentChunk;
    private long bytesRead;
    private Collection<String> usernameBuffer;
    
    public UsernameReader(File sourceFile, int chunkSize, int nextChunkIndex) throws FileNotFoundException {
        this.sourceFile = sourceFile;
        this.scanner = new Scanner(this.sourceFile);
        if (chunkSize <= 0) {
            throw new IllegalArgumentException("ChunkSize must be >= 1");
        }
        this.chunkSize = chunkSize;
        this.currentChunk = nextChunkIndex;
        this.usernameBuffer = new HashSet<>(this.chunkSize);
    }
    
    public UsernameReader(File sourceFile, int chunkSize, int nextChunkIndex, int startLineIndex)
            throws FileNotFoundException {
        this(sourceFile, chunkSize, nextChunkIndex);
        forwardToLine(startLineIndex);
    }
    
    public boolean hasNextChunk() {
        return this.scanner.hasNextLine();
    }
    
    public UsernameChunk nextChunk() throws ChunkReadException {
        if (this.scanner.ioException() != null) {
            throw new IllegalStateException("Scanner has IOException", this.scanner.ioException());
        }
        this.usernameBuffer.clear();
        IOException exception = null;
        while (this.scanner.hasNextLine() && this.usernameBuffer.size() < this.chunkSize && exception == null) {
            String line = this.scanner.nextLine();
            if (!line.isEmpty()) {
                this.usernameBuffer.add(line);
            }
            exception = this.scanner.ioException();
            this.currentLine++;
            this.bytesRead += line.getBytes().length;
        }
        UsernameChunk chunk = new UsernameChunk(this.currentChunk, this.usernameBuffer);
        this.currentChunk++;
        if (exception != null) {
            throw new ChunkReadException(exception, chunk);
        }
        return chunk;
    }
    
    public int getCurrentChunk() {
        return currentChunk;
    }
    
    public int getCurrentLine() {
        return currentLine;
    }
    
    public long getBytesRead() {
        return bytesRead;
    }
    
    public long getTotalBytes() {
        return sourceFile.length();
    }
    
    public void close() {
        if (this.scanner != null) {
            this.scanner.close();
        }
    }
    
    private void forwardToLine(int lineIndex) {
        while (this.currentLine < lineIndex && this.scanner.hasNextLine()) {
            // Just drop this line
            String line = this.scanner.nextLine();
            this.currentLine++;
            this.bytesRead += line.getBytes().length;
        }
    }
}
