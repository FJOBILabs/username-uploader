package de.fjobilabs.usernamedatabasetools.reader;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 16.12.2017 - 11:40:54
 */
public class ChunkReadException extends Exception {
    
    private static final long serialVersionUID = 1638832645316045727L;
    
    private UsernameChunk chunk;
    
    public ChunkReadException(Exception cause, UsernameChunk chunk) {
        super(cause);
        this.chunk = chunk;
    }
    
    public UsernameChunk getChunk() {
        return chunk;
    }
}
