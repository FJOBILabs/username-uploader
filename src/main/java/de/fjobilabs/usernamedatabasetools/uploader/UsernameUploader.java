package de.fjobilabs.usernamedatabasetools.uploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DecimalFormat;

import de.fjobilabs.usernamedatabasetools.database.DatabaseException;
import de.fjobilabs.usernamedatabasetools.database.DatabaseHandler;
import de.fjobilabs.usernamedatabasetools.reader.ChunkReadException;
import de.fjobilabs.usernamedatabasetools.reader.UsernameChunk;
import de.fjobilabs.usernamedatabasetools.reader.UsernameReader;
import de.fjobilabs.usernamedatabasetools.ui.UI;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 16.12.2017 - 11:20:21
 */
public class UsernameUploader {
    
    private UI ui;
    private DatabaseHandler databaseHandler;
    private UsernameReader usernameReader;
    private File lineIndexFile;
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");
    
    public UsernameUploader(UI ui, DatabaseHandler databaseHandler, File sourceFile)
            throws FileNotFoundException {
        this.ui = ui;
        this.databaseHandler = databaseHandler;
        this.lineIndexFile = new File(sourceFile.getPath() + ".lidx");
        int lastStoredLineIndex = getLastStoredLine();
        int chunkSize = databaseHandler.getConfiguration().getChunkSize();
        int nextChunkIndex;
        try {
            nextChunkIndex = databaseHandler.getMaxSectionId() + 1;
        } catch (DatabaseException e) {
            this.ui.showWarning("Failed to get next section ID! Using 0.");
            this.ui.showException(e);
            nextChunkIndex = 0;
        }
        if (lastStoredLineIndex != -1) {
            this.usernameReader = new UsernameReader(sourceFile, chunkSize, nextChunkIndex,
                    lastStoredLineIndex);
        } else {
            this.usernameReader = new UsernameReader(sourceFile, chunkSize, nextChunkIndex);
        }
    }
    
    public int getCurrentLine() {
        return this.usernameReader.getCurrentLine();
    }
    
    public void uploadUsernames() throws UsernameUploaderException {
        int usernames = 0;
        long globalStartTime = System.currentTimeMillis();
        long lastPrintTime = 0; // We want to print after first loop
        int chunkSize = this.databaseHandler.getConfiguration().getChunkSize();
        while (this.usernameReader.hasNextChunk()) {
            long startTime = System.currentTimeMillis();
            UsernameChunk chunk = null;
            int usernamesPerLoop = 0;
            try {
                chunk = this.usernameReader.nextChunk();
            } catch (ChunkReadException e) {
                ui.showError("Exception while reading chunk");
                ui.showException(e);
                // We try uploading the chunk even if there was an error
                chunk = e.getChunk();
            }
            if (chunk != null) {
                try {
                    uploadChunk(chunk);
                    saveLastStoredLine(this.usernameReader.getCurrentLine());
                } catch (DatabaseException e) {
                    throw new UsernameUploaderException("Failed to load chunk into database", e);
                }
                usernames += chunk.getSize();
                usernamesPerLoop = chunk.getSize();
            }
            long time = System.currentTimeMillis() - startTime;
            
            if (System.currentTimeMillis() - lastPrintTime > 1000) {
                double usernamesPerMinute = usernames / (time / (1000.0 * 60.0));
                System.out.println("Usernames: " + usernamesPerLoop + " @ "
                        + this.decimalFormat.format(usernamesPerMinute) + " names/minute (chunk size: "
                        + chunkSize + ") (" + this.decimalFormat.format(getProgress()) + "%)");
                lastPrintTime = System.currentTimeMillis();
            }
        }
        double totalTime = System.currentTimeMillis() - globalStartTime;
        int namesPerMinute = (int) (usernames / (totalTime / (1000.0 * 60.0)));
        ui.showInfo("Uploaded " + usernames + " usernames @ " + namesPerMinute
                + " names/minute with chunk size " + chunkSize);
        this.usernameReader.close();
    }
    
    private double getProgress() {
        return this.usernameReader.getBytesRead() / (double) this.usernameReader.getTotalBytes();
    }
    
    private void uploadChunk(UsernameChunk chunk) throws DatabaseException {
        this.databaseHandler.startBatch();
        for (String username : chunk.getUsernames()) {
            try {
                this.databaseHandler.addUsername(username, chunk.getId());
            } catch (DatabaseException e) {
                e.printStackTrace();
            }
        }
        this.databaseHandler.executeBatch();
    }
    
    private void saveLastStoredLine(long lineIndex) {
        try {
            Files.write(this.lineIndexFile.toPath(), Long.toString(lineIndex).getBytes());
        } catch (IOException e) {
            System.err.println("Failed to save last stored line");
            e.printStackTrace();
        }
    }
    
    private int getLastStoredLine() {
        if (!this.lineIndexFile.exists()) {
            return -1;
        }
        FileInputStream in = null;
        byte[] bytes = null;
        try {
            in = new FileInputStream(this.lineIndexFile);
            if (in.available() <= 0) {
                in.close();
                return -1;
            }
            bytes = new byte[in.available()];
            in.read(bytes);
        } catch (IOException e) {
            
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    System.err.println("Failed to close FileInputStream to line index file:");
                    e.printStackTrace();
                }
            }
        }
        
        String s = new String(bytes);
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}
