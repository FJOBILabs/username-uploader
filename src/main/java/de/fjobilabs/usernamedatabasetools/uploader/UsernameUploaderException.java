package de.fjobilabs.usernamedatabasetools.uploader;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 16.12.2017 - 12:25:04
 */
public class UsernameUploaderException extends Exception {
    
    private static final long serialVersionUID = 352768040527617280L;
    
    public UsernameUploaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
