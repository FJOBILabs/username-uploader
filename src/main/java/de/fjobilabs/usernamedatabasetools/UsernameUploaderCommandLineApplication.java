package de.fjobilabs.usernamedatabasetools;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

import de.fjobilabs.usernamedatabasetools.CommandLineApplication;
import de.fjobilabs.usernamedatabasetools.uploader.UsernameUploader;
import de.fjobilabs.usernamedatabasetools.uploader.UsernameUploaderException;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 21.12.2017 - 17:34:31
 */
public class UsernameUploaderCommandLineApplication extends CommandLineApplication {
    
    public UsernameUploaderCommandLineApplication() {
        super("username-uploader [OPTIONS] [PATH]");
    }
    
    @Override
    protected void addOptions(Options options) {
    }
    
    @Override
    protected void run(CommandLine commandLine) {
        if (commandLine.getArgList().size() == 0) {
            System.out.println("Missing: [PATH]");
            printHelp();
            System.exit(1);
            return;
        }
        
        String path = commandLine.getArgList().get(0);
        File file = new File(path);
        if (file.isDirectory()) {
            uploadDirectory(file);
        } else {
           uploadFile(file);
        }
    }
    
    private void uploadFile(File file) {
        if (!file.exists() || !file.isFile()) {
            System.out.println("PATH '" + file + "' is not a file");
            System.exit(1);
            return;
        }
        System.out.println("Uploading usernames form file: " + file);
        if (!uploadSingleFile(file)) {
            System.exit(1);
            return;
        }
        System.out.println("Finished!");
    }
    
    private void uploadDirectory(File directory) {
        if (!directory.exists() || !directory.isDirectory()) {
            System.out.println("PATH '" + directory + "' is not a directory");
            System.exit(1);
            return;
        }
        System.out.println("Uploading usernames in directory: " + directory);
        for (File file : directory.listFiles()) {
            if (file.isFile() && !file.getName().endsWith(".lidx")) {
                System.out.println("Uploading usernames from file: " + file);
                if (!uploadSingleFile(file)) {
                    System.out.println("Failed to upload file: " + file);
                }
            }
        }
        System.out.println("Finished!");
    }
    
    private boolean uploadSingleFile(File file) {
        UsernameUploader uploader;
        try {
            uploader = new UsernameUploader(getUi(), getDatabaseHandler(), file);
        } catch (FileNotFoundException e) {
            System.out.println("PATH '" + file + "' is not a file!");
            return false;
        }
        int startLine = uploader.getCurrentLine();
        if (startLine == 0) {
            System.out.println("Starting at beginning of the file...");
        } else {
            System.out.println("Continuing at line " + startLine + "...");
        }
        try {
            uploader.uploadUsernames();
        } catch (UsernameUploaderException e) {
            getUi().showError("Error while uploading usernames: " + e.getMessage());
            getUi().showException(e);
            return false;
        }
        return true;
    }
    
    public static void main(String[] args) {
        new UsernameUploaderCommandLineApplication().init(args);
    }
}
